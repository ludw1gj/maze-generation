import { Direction } from "./direction";

export interface Position {
  x: number;
  y: number;
}

export interface CellWalls {
  north: boolean;
  east: boolean;
  south: boolean;
  west: boolean;
}

export class Cell {
  private _position: Position;
  private _isVisited: boolean;
  private _walls: CellWalls;

  constructor(position: Position, isVisited?: boolean, walls?: CellWalls) {
    this._position = position;
    this._isVisited = isVisited !== undefined ? isVisited : false;
    this._walls = walls
      ? walls
      : {
          north: true,
          east: true,
          south: true,
          west: true,
        };
  }

  public createCopy(): Cell {
    return new Cell(this._position, this._isVisited, this._walls);
  }

  get position(): Position {
    return this._position;
  }

  get isVisited(): boolean {
    return this._isVisited;
  }

  get walls(): CellWalls {
    return this._walls;
  }

  public setVisited(): void {
    this._isVisited = true;
  }

  public destroyWall(direction: Direction): void {
    switch (direction) {
      case "NORTH": {
        this._walls.north = false;
        break;
      }
      case "EAST": {
        this._walls.east = false;
        break;
      }
      case "SOUTH": {
        this._walls.south = false;
        break;
      }
      case "WEST": {
        this._walls.west = false;
        break;
      }
      default:
        throw new Error("invalid direction given");
    }
  }

  public getChangeInPosition(dir: Direction): Position {
    const { x, y } = this._position;
    switch (dir) {
      case "NORTH": {
        return { x, y: y - 1 };
      }
      case "EAST": {
        return { x: x + 1, y };
      }
      case "SOUTH": {
        return { x, y: y + 1 };
      }
      case "WEST": {
        return { x: x - 1, y };
      }
      default:
        throw new Error("invalid direction given");
    }
  }
}
