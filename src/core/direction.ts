export type Direction = "NORTH" | "EAST" | "SOUTH" | "WEST";

export const DIRECTIONS: readonly Direction[] = ["NORTH", "EAST", "SOUTH", "WEST"];

export function getOppositeDirection(dir: Direction): Direction {
  switch (dir) {
    case "NORTH": {
      return "SOUTH";
    }
    case "EAST": {
      return "WEST";
    }
    case "SOUTH": {
      return "NORTH";
    }
    case "WEST": {
      return "EAST";
    }
    default:
      throw new Error("invalid direction given");
  }
}
