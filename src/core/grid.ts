import { Cell, Position } from "./cell";
import { Direction, DIRECTIONS } from "./direction";

export class Grid {
  private readonly cells: Cell[][];
  private readonly _height: number;
  private readonly _width: number;

  constructor(height: number, width: number) {
    const cells = new Array(height);
    for (let y = 0; y < cells.length; y++) {
      cells[y] = new Array(width);
      for (let x = 0; x < cells[y].length; x++) {
        cells[y][x] = new Cell({ x, y });
      }
    }
    this.cells = cells;
    this._height = height;
    this._width = width;
  }

  get height(): number {
    return this._height;
  }

  get width(): number {
    return this._width;
  }

  public getCell(pos: Position): Cell | undefined {
    if (this.isPositionValid(pos)) {
      return this.cells[pos.y][pos.x];
    }
    return undefined;
  }

  public exportCells(): Cell[][] {
    return this.cells.map(row => row.map(cell => cell.createCopy()));
  }

  public getUnvisitedNeighbours(cell: Cell): Array<{ neighbourCell: Cell; dir: Direction }> {
    return DIRECTIONS.map(dir => {
      const neighbourPos = cell.getChangeInPosition(dir);
      const neighbourCell = this.getCell(neighbourPos);
      return { neighbourCell, dir };
    }).filter(val => val.neighbourCell !== undefined && !val.neighbourCell.isVisited) as Array<{
      neighbourCell: Cell;
      dir: Direction;
    }>;
  }

  private isPositionValid(pos: Position): boolean {
    return pos.x >= 0 && pos.y >= 0 && pos.x < this._width && pos.y < this._height;
  }

  public static getRandomElement<T>(arr: T[]): T {
    const randIndex = Math.floor(Math.random() * arr.length);
    return arr[randIndex];
  }
}
